package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    final int[] dicesImagesRef = {
            R.drawable.dice_1,
            R.drawable.dice_2,
            R.drawable.dice_3,
            R.drawable.dice_4,
            R.drawable.dice_5,
            R.drawable.dice_6,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button rollButton = (Button) findViewById(R.id.rollButton);
        final ImageView dice1Image = (ImageView) findViewById(R.id.rollText);
        final ImageView dice2Image = (ImageView) findViewById(R.id.rollText2);
        Button resetButton = findViewById(R.id.resetButton);

        rollButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String text = "Dice Rolled";
                rollButton.setText(text);
                int rollResult = (int)(Math.random() * 6);

                boolean wasCritical = rollResult == 5;

                dice1Image.setImageResource(dicesImagesRef[rollResult]);

                rollResult = (int)(Math.random() * 6);

                if(wasCritical && rollResult == 5)
                {
                    Toast notification = Toast.makeText(MainActivity.this, "Jackpot", Toast.LENGTH_SHORT);
                    notification.setGravity(Gravity.TOP,0,0);
                    notification.show();
                }

                dice2Image.setImageResource(dicesImagesRef[rollResult]);
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dice1Image.setImageResource(R.drawable.empty_dice);
                dice2Image.setImageResource(R.drawable.empty_dice);
                rollButton.setText(R.string.button);
            }
        });

        dice1Image.setOnClickListener(this);
        dice2Image.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        ImageView dice = (ImageView)v;
        int rollResult = (int)(Math.random() * 6);
        dice.setImageResource(dicesImagesRef[rollResult]);
    }
}